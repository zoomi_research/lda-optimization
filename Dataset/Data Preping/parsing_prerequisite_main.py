import pickle
import math

segment_length = 20

def remove_RS(action_sequences):
    users = action_sequences.keys()
    for user in users:
        contents = action_sequences[user].keys()
        for content in contents:
            for id, action_list in enumerate(action_sequences[user][content][0]):
                if action_list['action'] == 'RS':
                    del action_sequences[user][content][0][id]
    return action_sequences


def expand_motif(action_sequences):
    result_list = {}
    users = action_sequences.keys()
    for user in users:
        result_list[user]={}
        contents = action_sequences[user].keys()
        for content in contents:
            result_list[user][content]=[]
            for item in action_sequences[user][content][0]:
                for iter in range(item['time_elapsed']):
                    result_list[user][content].append(item['action'])
    return result_list

def check_content(action_sequences):
    users = action_sequences.keys()
    for user in users:
        contents = action_sequences[user].keys()
        print contents[0],len(action_sequences[user][contents[0]])

def replace_skip(action_sequences,skip_sequences):
    users = skip_sequences.keys()
    for user in users:
        if user not in action_sequences.keys(): continue
        contents = skip_sequences[user].keys()
        for content in contents:
            if content not in action_sequences[user].keys(): continue
            for skip_items in skip_sequences[user][content][0]:
                to_replace = int(math.floor(skip_items['position']/segment_length))
                if to_replace<len(action_sequences[user][content]):
                    action_sequences[user][content][to_replace] = skip_items['action']
    return action_sequences





with open('feature_sequence_ITM_GL_GL_RRP_030_1491403560000.pickle', 'rb') as handle:
    action_sequences = pickle.load(handle)
with open('event_sequence_ITM_GL_GL_RRP_030_1491403560000.pickle', 'rb') as handle:
    skip_sequences = pickle.load(handle)

'''remove Repeating motif '''
action_sequences = remove_RS(action_sequences)

'''expand the motif'''
action_sequences = expand_motif(action_sequences)

'''replace skip'''
action_sequences = replace_skip(action_sequences,skip_sequences)

''' save '''
with open('outcome_variable_ITM_GL_GL_RRP_030_1491403560000.pickle', 'wb') as handle:
    pickle.dump(action_sequences, handle, protocol=pickle.HIGHEST_PROTOCOL)
