


def plot_action_times_for_slideIDs(actions_dict_user_slideID_sequences, slideIDs_durations):

    slideID_acts = {}
    for sID in slideIDs_durations:
        slideID_acts[sID] = []

        for u in actions_dict_user_slideID_sequences:
            if sID in actions_dict_user_slideID_sequences[u]:
                all_seq = actions_dict_user_slideID_sequences[u][sID]
                for seq in all_seq:
                    for act in seq:
                        slideID_acts[sID].append(act)

    for sID in slideID_acts:
        play_times = []
        pause_times = []
        sf_times = []
        sb_times = []
        for act in slideID_acts[sID]:
            if act['type'] == 'Play':
                play_times.append(act['time_elapsed'])
            if act['type'] == 'Pause':
                pause_times.append(act['time_elapsed'])
            if act['type'] == 'Scrubb' and act['time_elapsed'] > 0:
                sf_times.append(act['time_elapsed'])
            if act['type'] == 'Scrubb' and act['time_elapsed'] < 0:
                sb_times.append(act['time_elapsed'])

        to_plot = [play_times, pause_times, sf_times, sb_times]
        to_plot_name = ['Play', 'Pause', 'Sf', 'Sb']


        if len(to_plot) > 0:
            plt.figure()
            plt.boxplot(to_plot)
            plt.xticks(xrange(1, len(to_plot)+1), to_plot_name )
            plt.ylim([-10,200])
            plt.ylabel('time_elapsed')
            plt.xlabel('type')
            plt.savefig('boxplots/boxplot_time_elapsed_%s_pdf' %sID)

    return 0

