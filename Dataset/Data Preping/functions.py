m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


'''/////////////////////////////////////  supportive func to parse ///////////////////////////////////////////////'''

def get_evt_from_list(events, measure):
    out = []
    for evt in events:
        if evt['m_type'] == measure:
            out.append(evt)
    return out



def get_uv_dict(events):
    uv = {}
    for evt in events:
        if evt['user'] not in uv:
            uv[ evt['user'] ] = [evt]
        else:
            uv[ evt['user'] ].append(evt)

    for user in uv:
        uv[user] = sorted(uv[user], key=lambda k: k['timestamp'])

    return uv



def make_valid(uv):

    for user in uv: # make each user's clickstream events valid
        results = uv[user]
        results = filter_duplicates(results)
        results = filter_nonsense(results)
        uv[user] = results

    return uv


def filter_duplicates(events):  # a list of events from the same person

    previous = {'m_type': 'none', 'type':'none', 'timestamp':0.0, 'position':-1000}

    new = []

    for event in events:
        dup = False
        if 'm_type' in event and event['m_type'] == previous['m_type']:


            if 'type' in event:
                if event['type'] == 'Pause' and previous['type'] == 'Play':
                    if event['position'] == previous['position']:
                        if len(new) > 0:
                            new.pop()
                            previous = event
                            continue


            if 'type' in event and event['type'] == previous['type']:
                if 'position' in event and event['position'] == previous['position']:
                    dup = True


        if not dup:
            new.append(event)


        previous = event

    del events

    return new



def filter_nonsense(events):  # a list of events from the same person

    return events




def get_chapEvt_dict_and_lifeCycle(evts): # for just one user.  evts = all events of one user

    chaps = {}
    all_lc = []
    for evt in evts:

        if evt['m_type'] == 'SearchMeasurement':
            continue
        elif evt['m_type'] == 'LifeCycleMeasurement':
            all_lc.append(evt)
            continue


        if 'chapter' not in evt:
            continue
        else:
            if evt["chapter"] not in chaps:
                chaps[ evt['chapter'] ] = [evt]
            else:
                chaps[ evt['chapter'] ].append( evt )

    return chaps, all_lc




def get_existing_types(events):
    types = []

    for evt in events:
        if evt['m_type'] not in types:
            types.append(evt['m_type'])

    return types


def get_num_sessions(all_evts):

    sessionIDs = []

    for evt in all_evts:
        if evt['session'] not in sessionIDs:
            sessionIDs.append(evt['session'])

    return len(sessionIDs)





def get_numSignIn(nav_evts):

    num = 0
    for evt in nav_evts:
        if evt['action'] == "open":
            num += 1

    return num



def get_counts(storyLine_evts):

    user = storyLine_evts.keys()[0]
    storyLine_evts = storyLine_evts[user]

    [numPa, numSb, numSf, numRe] = [0, 0, 0, 0]

    for evt in storyLine_evts:
        if evt["type"] == "Pause":
            numPa += 1
        elif evt["type"] == "Replay":
            numRe += 1
        elif evt["type"] == "Scrubb":
            if evt["toPosition"] > evt["position"]:
                numSf += 1
            elif evt["toPosition"] < evt["position"]:
                numSb += 1
            #else:
                #print("position == toPosition: %s" %evt)

    return numPa, numSb, numSf, numRe



def get_numMaxMinWindow(window_evts, existing_types):  # window_evts: from just one user and one chapter

    windowInfo = {}

    if m_article in existing_types:
        windowInfo['epub'] = []
    if m_pdf in existing_types:
        windowInfo['pdf'] = []
    if m_video in existing_types:
        windowInfo['video'] = []
    if m_slide in existing_types:
        windowInfo['storyline'] = []

    for evt in window_evts:

        total_width = evt['width']
        total_height = evt['height']

        for comp in evt['components']:

            if comp['width'] > total_width - 5 and comp['height'] > total_height - 5: # this mode is maximized
                if comp['componentType'] not in windowInfo:
                    windowInfo[ comp['componentType'] ] = ['max']
                else:
                    windowInfo[ comp['componentType'] ].append('max')

            elif comp['height'] < 40:  # this mode is minimized
                if comp['componentType'] not in windowInfo:
                    windowInfo[ comp['componentType'] ] = ['min']
                else:
                    windowInfo[ comp['componentType'] ].append('min')

            else: # nothing happens
                if comp['componentType'] not in windowInfo:
                    windowInfo[ comp['componentType'] ] = ['none']
                else:
                    windowInfo[ comp['componentType'] ].append('none')



    for mode in windowInfo:

        if len(windowInfo[mode]) == 0:
            windowInfo[mode] = {}
            windowInfo[mode]['numMax'] = 0
            windowInfo[mode]['numMin'] = 0

        elif len(windowInfo[mode]) == 1:
            if windowInfo[mode][0] == 'max':
                windowInfo[mode] = {'numMax': 1, 'numMin': 0}
            elif windowInfo[mode][0] == 'min':
                windowInfo[mode] = {'numMax': 0, 'numMin': 1}
            else:
                windowInfo[mode] = {'numMax': 0, 'numMin': 0}


        elif len(windowInfo[mode]) > 1:
            numMax, numMin = 0, 0
            if windowInfo[mode][0] == 'max':
                numMax += 1
            elif windowInfo[mode][0] == 'min':
                numMin += 1

            for i in xrange(1, len(windowInfo[mode])):
                if windowInfo[mode][i] == 'max' and windowInfo[mode][i-1] != 'max':
                    numMax += 1
                elif windowInfo[mode][i] == 'min' and windowInfo[mode][i-1] != 'min':
                    numMin += 1

            windowInfo[mode] = {'numMax': numMax, 'numMin': numMin}

    return windowInfo






def get_scroll_count_article(art_evts):
    numScroll_up, numScroll_down = 0, 0

    if len(art_evts) >= 2:
        for i in xrange(1, len(art_evts)):
            if art_evts[i]['startIndex'] > art_evts[i-1]['startIndex']:
                numScroll_down += 1
            elif art_evts[i]['startIndex'] < art_evts[i-1]['startIndex']:
                numScroll_up += 1

    return numScroll_up, numScroll_down






def get_scroll_count_pdf(pdf_evts):

    numScroll_up, numScroll_down = 0, 0

    if len(pdf_evts) >= 2:
        for i in xrange(1, len(pdf_evts)):
            if pdf_evts[i]['page'] > pdf_evts[i-1]['page']:
                numScroll_down += 1
            elif pdf_evts[i]['page'] < pdf_evts[i-1]['page']:
                numScroll_up += 1

    return numScroll_up, numScroll_down



def get_num_notes(evts_thisChap):

    return 0

def get_num_bookmarks(evts_thisChap):

    return 0

def get_num_highlights(evts_thisChap):

    return 0


def attr_dict_to_list(attr, attr_dict):  # attr_dict of this user

    row = []
    for item in attr:
        val = 0 # initiate it to be zero
        x = item.split("$$")

        if x[0][0:10] == 'engagement':
            break

        if len(x) == 3:
            [ch, mode, meas] = x[0:3]
            if ch in attr_dict:
                if mode in attr_dict[ch]:
                    if meas in attr_dict[ch][mode]:
                        val = attr_dict[ch][mode][meas]
                    else:
                        val = 0
        elif len(x) == 2:
            [ch, meas] = x[0:2]
            if ch in attr_dict:
                if meas in attr_dict[ch]:
                    val = attr_dict[ch][meas]
                else:
                    val = 0
        elif len(x) == 1:
            meas = x[0]
            if meas in attr_dict:
                val = attr_dict[meas]
            else:
                val = 0
        else:
            print("error with attr:  %s" %item)

        row.append(val)

    return row







# def get_engagements(uv):
#     timeSpent_info = {}
#     weights = {}
#     max_ts = {}
#
#     storylineLens = get_slides_total_duration(events)
#
#     for user in uv:
#         timeSpent_info[user] = {}
#
#         all_evts = uv[user]
#         chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts)
#         all_lc = {user: all_lc}
#
#         for ch in chap_names:
#             if ch not in max_ts:
#                 max_ts[ch] = {}
#
#             if ch not in chapters:
#                 continue
#
#             timeSpent_info[user][ch] = {}
#             if ch not in weights:
#                 weights[ch] = {}
#
#             evts_thisChap = chapters[ch]
#             existing_types = func.get_existing_types(evts_thisChap)
#             nav_evts = func.get_evt_from_list(evts_thisChap, m_navi)
#             nav_evts = {user: nav_evts}
#
#             window_evts = func.get_evt_from_list(evts_thisChap, m_window)
#             window_evts = {user: window_evts}
#
#             if m_article in existing_types:
#                 if 'article' not in max_ts[ch]:
#                     max_ts[ch]['article'] = 0
#
#                 art_evts = func.get_evt_from_list(evts_thisChap, m_article)
#                 weight = (art_evts[0]["totalCharacters"] / 200.0)  # get the weight for computing engagement
#                 weights[ch]['article'] = weight
#                 art_evts = {user: art_evts}
#
#                 result = Measurements_article.get_measurements(art_evts, nav_evts, window_evts, all_lc)
#                 art_compRate = result['averageCompletionRate'] # average for single user is just this user's rate
#                 art_timeSpent = result['totalTimeSpent']
#
#                 timeSpent_info[user][ch]['article'] = art_timeSpent
#                 if art_timeSpent > max_ts[ch]['article']:
#                     max_ts[ch]['article'] = art_timeSpent
#
#
#             if m_pdf in existing_types:
#                 if 'pdf' not in max_ts[ch]:
#                     max_ts[ch]['pdf'] = 0
#
#                 pdf_evts = func.get_evt_from_list(evts_thisChap, m_pdf)
#                 weight = pdf_evts[0]["totalPages"]  # get the weight for computing engagement
#                 weights[ch]['pdf'] = weight
#
#                 pdf_evts = {user: pdf_evts}
#                 result, dummy = Measurements_pdf.get_measurements(pdf_evts, nav_evts, window_evts, all_lc)
#                 pdf_compRate = result['averageCompletionRate']
#                 pdf_timeSpent = result['totalTimeSpent']
#
#                 timeSpent_info[user][ch]['pdf'] = pdf_timeSpent
#                 if pdf_timeSpent > max_ts[ch]['pdf']:
#                     max_ts[ch]['pdf'] = pdf_timeSpent
#
#             if m_slide in existing_types:
#                 if 'storyline' not in max_ts[ch]:
#                     max_ts[ch]['storyline'] = 0
#
#                 storyLine_evts = func.get_evt_from_list(evts_thisChap, m_slide)
#                 weights[ch]['storyline'] = storylineLens[ch]
#                 storyLine_evts = {user: storyLine_evts}
#                 slideChanged = func.get_evt_from_list(evts_thisChap, m_slideChanged)
#                 slideChanged = {user: slideChanged}
#                 slideComp = func.get_evt_from_list(evts_thisChap, m_slideCompleted)
#                 slideComp = {user: slideComp}
#
#                 result = Measurements_storyline.get_measurements(storyLine_evts, nav_evts, slideChanged, slideComp,
#                                                                  window_evts, all_lc)
#                 storyline_compRate = result['averageCompletionRate']
#                 storyline_timeSpent = result['totalTimeSpent']
#
#                 timeSpent_info[user][ch]['storyline'] = storyline_timeSpent
#                 if storyline_timeSpent > max_ts[ch]['storyline']:
#                     max_ts[ch]['storyline'] = storyline_timeSpent
#
#     # averaged over the max from all users ( for each mode )
#     for user in timeSpent_info:
#         for ch in timeSpent_info[user]:
#             for mode in timeSpent_info[user][ch]:
#                 if float(max_ts[ch][mode]) != 0:
#                     timeSpent_info[user][ch][mode] = 100 * (timeSpent_info[user][ch][mode] / float(max_ts[ch][mode]))
#
#     engagements = {}
#     for user in timeSpent_info:
#         engagements[user] = {}
#         for ch in timeSpent_info[user]:
#             eng = 0
#             divider = 0
#
#             for mode in timeSpent_info[user][ch]:
#                 eng += timeSpent_info[user][ch][mode] * weights[ch][mode]
#                 divider += 1
#
#             if divider == 0:
#                 engagements[user][ch] = 0
#             else:
#                 engagements[user][ch] = eng/float(divider)
#
#
#
#     return  engagements
