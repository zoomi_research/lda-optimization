import pymongo
import MySQLdb
import pandas as pd
import math
import logging


DATE_TIME_FORMAT = '%Y-%m-%d--%H-%M-%S'
DATE_TIME_FORMAT_HUMAN = '%B %d, %Y'
LIFECYCLE = "LifeCycleMeasurement"
OPEN = 'open'
CLOSE = 'close'
BACKGROUND = 'backgroundTab'
FOREGROUND = 'foregroundTab'
COMPANY = "14b8e7ee-3f75-4f4d-aac7-5a0bbab74f31"
ALPHA = .8


class Connection(object):
    def __init__(self, db):
        self.db = db

    def mongo_connect(self):
        self._do_connect('mongo_backend')

    def mongo_datamart_connect(self):
        self._do_connect('mongo_datamart')

    def mysql_connect(self):
        self._do_connect_mysql('mysql_backend')

    def _do_connect(self, connection_name):
        options = self.db[connection_name]
        if not self.db[connection_name]['CONNECTION']:
            try:
                if options['REPLICASET'] == "":
                    connection = pymongo.MongoClient(options['HOST'], options['PORT'])
                else:
                    connection = pymongo.MongoClient(options['HOST'], options['PORT'], replicaset=options['REPLICASET'])
                connection[options['NAME']].authenticate(options['USER'], options['PASSWORD'])

            except (SystemError, StandardError, pymongo.errors.OperationFailure), connection_error:
                raise ValueError('Can`t connect to %s' % connection_name, connection_error)

            self.db[connection_name]['CONNECTION'] = connection

    def _do_connect_mysql(self, connection_name):
        options = self.db[connection_name]
        if not self.db[connection_name]['CONNECTION']:
            try:
                connection = MySQLdb.Connection(
                    host=options['HOST'],
                    user=options['USER'],
                    passwd=options['PASSWORD'],
                    port=options['PORT'],
                    db=options['DB']
                )

            except (SystemError, StandardError), connection_error:
                raise ValueError('Can`t connect to %s' % connection_name, connection_error)

            self.db[connection_name]['CONNECTION'] = connection


class DashboardCalculation(Connection):
    def __init__(self, db, courses, runtime, path,course_level=False,file_level=False):
        super(DashboardCalculation, self).__init__(db)
        self.courses = [course for course in courses if len(course) > 0]
        self.runtime = runtime
        self.path = path
        self.collection = None
        self.csv = "{path}/dashboard-course.csv".format(path=self.path)
        self.csv_mode = "a"
        self.course_level = course_level
        self.file_level = file_level

        LOG_FILENAME = "{path}/errors_log_DashboardCalculation.out".format(path=self.path)
        logging.basicConfig(filename=LOG_FILENAME)


    def _create_base_pipeline(self, course, company=None, topic=None, chapters=None, chapter=None, content=None,
                              contents=None,
                              segment=None, question=None, user=None, user_ids=None, start=None, end=None):
        pipeline = []

        match = {}
        if company is not None:
            match['company'] = company

        if course is not None:
            match['course'] = course

        if user is not None:
            match['user'] = user.id
        if user_ids is not None:
            match['user'] = {'$in': user_ids}

        if topic is not None:
            match['topic'] = topic.id

        if chapter is not None:
            match['chapter'] = chapter.id
        if chapters is not None:
            match['chapter'] = {'$in': [chap.id for chap in chapters]}

        if content is not None:
            match['content'] = content
        if contents is not None:
            match['content'] = {'$in': [con.id for con in contents]}

        if segment is not None:
            match['segment'] = segment

        if question is not None:
            match['question'] = question.id

        if start is not None:
            match['start_timestamp'] = {'$gte': start}
        if end is not None:
            match['end_timestamp'] = {'$lte': end}

        pipeline = [{'$match': match}]
        return pipeline

    def user_course_list(self, mysql, collection, course):
        if isinstance(course, basestring):
            courses = [course]
        else:
            courses = self.courses
        # Get all of the valid users with their external id
        cursor = mysql.cursor()
        sql = 'SELECT id,externalID,user_id FROM user_management_miicuser'
        cursor.execute(sql)
        result = cursor.fetchall()
        valid_user_ids = []
        for res in result:
            valid_user_ids.append(res[0])
        user_df = pd.DataFrame([[ij for ij in i] for i in result])
        user_df.rename(columns={0: "user_id", 1: "external_id", 2: "auth_id"}, inplace=True)

        user_course_df = []
        # Get the users with clickstream events in measure_base
        for course in courses:
            users = collection.find({'course': course}).distinct('user')
            valid_course_users = set(users).intersection(valid_user_ids)
            user_records = [(user, course) for user in valid_course_users]
            labels = ['user_id', 'course_id']
            if len(user_course_df) > 0:
                user_course_df = pd.concat([user_course_df, pd.DataFrame.from_records(user_records, columns=labels)])
            else:
                user_course_df = pd.DataFrame.from_records(user_records, columns=labels)
        # get the external id
        final_user_df = pd.merge(user_course_df, user_df, how='left', on='user_id')

        # For quiz completion, we should also be adding first and last name
        cursor = mysql.cursor()
        sql = 'SELECT id,first_name,last_name FROM auth_user'
        cursor.execute(sql)
        result = cursor.fetchall()
        user_info_df = pd.DataFrame([[ij for ij in i] for i in result])
        user_info_df.rename(columns={0: "auth_id", 1: "first_name", 2: "last_name"}, inplace=True)
        final_user_info_df = pd.merge(final_user_df, user_info_df, how='left', on='auth_id')
        return final_user_info_df, len(valid_course_users)

    def fetch_course_unit_content(self, db, course):
        if isinstance(course, basestring):
            courses = [course]
        else:
            courses = self.courses

        cursor = db.cursor()
        sql = 'SELECT id,title FROM course_manager_course WHERE id IN (%s)'
        in_p = ', '.join(map(lambda x: '%s', courses))
        sql = sql % in_p
        cursor.execute(sql, courses)
        result = cursor.fetchall()

        sql = 'SELECT id FROM course_manager_topic WHERE course_id IN (%s)'
        in_p = ', '.join(map(lambda x: '%s', courses))
        sql = sql % in_p
        cursor.execute(sql, courses)
        result = cursor.fetchall()
        topic_ids = []
        for row in result:
            # print row
            topic_ids.append(row[0])
        if len(topic_ids) < 1:
            print '{course} has no topics in the database. It will be skipped in this analysis.'.format(course=course)
            return [],[]

        print 'TOPICS',topic_ids
        sql = 'SELECT id FROM course_manager_chapter WHERE topic_id IN (%s)'
        in_p = ', '.join(map(lambda x: '%s', topic_ids))
        sql = sql % in_p
        cursor.execute(sql, topic_ids)
        result = cursor.fetchall()
        chapter_ids = []
        for row in result:
            # print row
            chapter_ids.append(row[0])

        if len(chapter_ids) < 1:
            print '{course} has no chapters in the database. It will be skipped in this analysis.'.format(course=course)
            return [],[]

        sql = 'SELECT id,type FROM course_manager_content WHERE chapter_id IN (%s)'
        in_p = ', '.join(map(lambda x: '%s', chapter_ids))
        sql = sql % in_p
        cursor.execute(sql, chapter_ids)
        result = cursor.fetchall()
        content_ids = []
        for row in result:
            # print row
            content_ids.append(row)
        return chapter_ids, content_ids

    # @profile
    def get_user_course_timespent(self, db, course, user_by_course):
        users = user_by_course.loc[:, 'user_id']
        user_by_course['timespent'] = 0
        start = time.time()
        test = list(db.find({'course': 'ITM_GL_GL_RRP_031_1491403620000',
                             'user': '34c7a2a0-e303-4c67-a4f3-438ffe777e3f'}))

        print "test time", time.time() - start
        for user in users[0:5]:
            print user
            print

            user_events = db.find({'course': course,
                                   'user': user}).sort('timestamp', 1)

            user_timespent = 0
            user_timespent_away = 0
            open_timestamp = None
            background_timestamp = None
            background = False
            currently_counting = False
            for event in user_events:

                final_ts = event.get('timestamp')
                if event.get('m_type') == LIFECYCLE and event.get('action') == OPEN:
                    if not currently_counting:
                        open_timestamp = event.get('timestamp')
                        currently_counting = True
                elif event.get('m_type') == LIFECYCLE and event.get('action') == CLOSE:
                    if background:
                        user_timespent_away += event.get('timestamp') - background_timestamp
                        background_timestamp = None
                        background = False

                    if currently_counting:
                        # Is the timespent really long?
                        if event.get('timestamp') - open_timestamp > 1500:
                            # Is the last event greater than two minutes away
                            if event.get('timestamp') - prev_ts > 120:
                                user_timespent += event.get('timestamp') - prev_ts
                            else:
                                user_timespent += event.get('timestamp') - open_timestamp
                            open_timestamp = None
                            currently_counting = False
                        else:
                            user_timespent += event.get('timestamp') - open_timestamp
                            open_timestamp = None
                            currently_counting = False

                elif event.get('m_type') == LIFECYCLE and event.get('action') == BACKGROUND:
                    if not background:
                        background_timestamp = event.get('timestamp')
                        background = True
                elif event.get('m_type') == LIFECYCLE and event.get('action') == FOREGROUND:
                    if background:
                        user_timespent_away += event.get('timestamp') - background_timestamp
                        background_timestamp = None
                        background = False
                prev_ts = final_ts

            # Clean up unfinished events
            if currently_counting and open_timestamp:
                user_timespent += final_ts - open_timestamp
            if background and background_timestamp:
                user_timespent_away += final_ts - background_timestamp

            total_user_timespent = user_timespent - user_timespent_away

            user_by_course.loc[(user_by_course.user_id == user) & (
                user_by_course.course_id == course), 'timespent'] = total_user_timespent

        print user_by_course.head(20)

    def user_course_timespent(self, datamart, course, user_by_course):
        course_collection = datamart['user_course_interactions']
        users = user_by_course.loc[:, 'user_id']
        # user_by_course['timespent'] = 0
        pipeline = self._create_base_pipeline(course, COMPANY, user_ids=list(users)) # check1
        pipeline.append({"$group": {"_id": "$user", "total_time": {"$sum": "$time_spent"}}})

        results = course_collection.aggregate(pipeline, allowDiskUse=True)
        dict_results = dict([r['_id'], {"time_spent": r['total_time']}] for r in results)
        timespent_df = pd.DataFrame.from_dict(dict_results, orient='index')

        user_by_course = user_by_course.merge(timespent_df, how='left', left_on='user_id', right_index=True)
        user_by_course = user_by_course.sort_values(by='user_id')

        return user_by_course

    def user_content_timespent(self, datamart, course, content, user_by_course):
        course_collection = datamart['user_content_interactions']
        users = user_by_course.loc[:, 'user_id']

        timespent_df = pd.DataFrame(index=users)

        first = True
        for file in content:
            pipeline = self._create_base_pipeline(course, COMPANY, content=file[0], user_ids=list(users))
            pipeline.append({"$group": {"_id": "$user", "total_time": {"$sum": "$time_spent"}}})

            results = course_collection.aggregate(pipeline, allowDiskUse=True)
            dict_results = dict([r['_id'], {file[0] + "_time_spent": r['total_time']}] for r in results)
            # if first:
            #     timespent_df = pd.DataFrame.from_dict(dict_results, orient='index')
            #     first = False
            # else:
            temp_df = pd.DataFrame.from_dict(dict_results, orient='index')
            timespent_df = timespent_df.merge(temp_df, how='left', left_index=True, right_index=True)

        user_by_course = user_by_course.merge(timespent_df, how='left', left_on='user_id', right_index=True)
        user_by_course = user_by_course.sort_values(by='user_id')
        return user_by_course

    def user_course_completion(self, datamart, course, user_course_calculation, content):
        segment_interactions = datamart['user_segment_interactions']
        content_interactions = datamart['user_content_interactions']
        users = user_course_calculation.loc[:, 'user_id']

        completion_df = pd.DataFrame(index=users)

        # First get number of segments in each piece of content
        content_segments = {}
        first = True
        for file in content:
            all_segment_interactions = content_interactions.find({'course': course, 'content': file[0]}).distinct(
                'number_of_segments')
            if len(all_segment_interactions) > 0:
                num_segments = max(all_segment_interactions)
            else:
                continue

            content_segments[file[0]] = num_segments

            pipeline = self._create_base_pipeline(course, COMPANY, content=file[0], user_ids=list(users))
            pipeline.append({'$group': {"_id": "$user", 'num_segments': {'$sum': '$number_of_segment_interactions'}}})
            results = content_interactions.aggregate(pipeline, allowDiskUse=True)
            dict_results = dict(
                [r['_id'], {file[0]: min(100, (100 * r['num_segments']) / float(num_segments))}] for r in results)
            # if first:
            #     completion_df = pd.DataFrame.from_dict(dict_results, orient='index')
            #     first = False
            # else:
            temp_df = pd.DataFrame.from_dict(dict_results, orient='index')
            completion_df = completion_df.merge(temp_df, how='left', left_index=True, right_index=True)

        total_segments = sum(content_segments.values())
        content_percentages = {key: content_segments[key] / float(total_segments) for key in content_segments}

        for content in content_percentages:
            completion_df[content] = completion_df[content] * content_percentages[content]

        completion_df['completion_rate'] = completion_df.sum(axis=1)
        user_course_calculation = user_course_calculation.merge(completion_df, how='left', left_on='user_id',
                                                                right_index=True)
        return user_course_calculation

    def user_content_completion(self, datamart, course, user_content_calculation, content):
        content_interactions = datamart['user_content_interactions']
        users = user_content_calculation.loc[:, 'user_id']

        completion_df = pd.DataFrame(index=users)

        # First get number of segments in each piece of content
        content_segments = {}
        first = True
        for file in content:
            all_segment_interactions = content_interactions.find({'course': course, 'content': file[0]}).distinct(
                'number_of_segments')
            if len(all_segment_interactions) > 0:
                num_segments = max(all_segment_interactions)
            else:
                continue

            content_segments[file[0]] = num_segments

            pipeline = self._create_base_pipeline(course, COMPANY, content=file[0], user_ids=list(users))
            pipeline.append({'$group': {"_id": "$user", 'num_segments': {'$sum': '$number_of_segment_interactions'}}})
            results = content_interactions.aggregate(pipeline, allowDiskUse=True)
            dict_results = dict(
                [r['_id'], {file[0] + '_completion_rate': min(100, (100 * r['num_segments']) / float(num_segments))}]
                for r in results)
            # if first:
            #     completion_df = pd.DataFrame.from_dict(dict_results, orient='index')
            #     first = False
            # else:
            temp_df = pd.DataFrame.from_dict(dict_results, orient='index')
            completion_df = completion_df.merge(temp_df, how='left', left_index=True, right_index=True)

        user_content_calculation = user_content_calculation.merge(completion_df, how='left', left_on='user_id',
                                                                  right_index=True)
        return user_content_calculation

    def user_course_engagement(self, datamart, course, user_course_calculation, content):

        # First get file level time spent
        content_interactions = datamart['user_content_interactions']
        users = user_course_calculation.loc[:, 'user_id']
        first = True
        content = [file for file in content if file[1] == 'video']
        if len(content) == 0:
            user_course_calculation['course_engagement'] = 0
            return user_course_calculation
        for file in content:
            all_segment_interactions = content_interactions.find({'course': course, 'content': file[0]}).distinct(
                'number_of_segments')
            if len(all_segment_interactions) > 0:
                num_segments = max(all_segment_interactions)
            else:
                continue

            pipeline = self._create_base_pipeline(course, COMPANY, content=file[0], user_ids=list(users))
            pipeline.append({"$group": {"_id": "$user", "total_time": {"$sum": "$time_spent"}}})
            results = content_interactions.aggregate(pipeline, allowDiskUse=True)
            dict_results = dict(
                [r['_id'], {file[0] + '_ts': math.pow((1 + r['total_time'] / float(num_segments)), ALPHA)}] for r in
                results)
            if first:
                ts_df = pd.DataFrame.from_dict(dict_results, orient='index')
                first = False
            else:
                temp_df = pd.DataFrame.from_dict(dict_results, orient='index')
                ts_df = ts_df.merge(temp_df, how='left', left_index=True, right_index=True)

        user_course_calculation = user_course_calculation.merge(ts_df, how='left', left_on='user_id',
                                                                right_index=True)
        for file in content:
            if not file[0] in user_course_calculation:
                print "The column {column} is not in the DataFrame.".format(column=file[0])
                continue
            user_course_calculation[file[0] + '_engagement'] = (user_course_calculation[file[0]] *
                                                                user_course_calculation[file[0] + '_ts']) / math.pow(2,
                                                                                                                     ALPHA)

        columns = list(user_course_calculation)
        engagement_columns = [col for col in columns if 'engagement' in col]
        user_course_calculation['course_engagement'] = user_course_calculation[engagement_columns].sum(axis=1).clip(
            upper=100.0)
        return user_course_calculation

    def user_content_engagement(self, datamart, course, user_content_calculation, content):

        # First get file level time spent
        content_interactions = datamart['user_content_interactions']
        users = user_content_calculation.loc[:, 'user_id']
        first = True
        content = [file for file in content if file[1] == 'video']
        if len(content) == 0:
            return user_content_calculation
        for file in content:
            all_segment_interactions = content_interactions.find({'course': course, 'content': file[0]}).distinct(
                'number_of_segments')
            if len(all_segment_interactions) > 0:
                num_segments = max(all_segment_interactions)
            else:
                continue

            user_content_calculation[file[0] + '_engagement'] = ((user_content_calculation[
                                                                     file[0] + '_completion_rate'] * (1 + (
                user_content_calculation[file[0] + '_time_spent'] / num_segments) ** ALPHA)) / math.pow(2, ALPHA)).clip(upper=100.0)

        return user_content_calculation

    def run(self):
        
        errors = []
        
        print
        print "* " * 50
        print "Running Dashboard Calculations for {date} on {courses} courses.".format(
            date=self.runtime.strftime(DATE_TIME_FORMAT_HUMAN), courses=len(self.courses))

        self.mongo_connect()
        connection = self.db['mongo_backend']['CONNECTION'][self.db['mongo_backend']['NAME']]
        self.collection = connection['measure_base']

        self.mongo_datamart_connect()
        datamart_connection = self.db['mongo_datamart']['CONNECTION'][self.db['mongo_datamart']['NAME']]
        self.datamart_collection = datamart_connection

        self.mysql_connect()
        mysql_connection = self.db['mysql_backend']['CONNECTION']

        if self.course_level:

            print
            print "* " * 50
            print " Running course level dashboard calculations on the following courses:"
            # Course level calculations
            first = True
            for course in self.courses:
                print 'Course:',course
                try:
                    units, content = self.fetch_course_unit_content(mysql_connection, course)
                    if len(content) == 0:
                        print "*" * 50
                        print "Course {course} has no existing content in the DB. Skipping.".format(course=course)
                        print "*" * 50
                        continue
                    user_by_course, num_valid_users = self.user_course_list(mysql_connection, self.collection, course)
                    if num_valid_users < 1:
                        print "*" * 50
                        print "Course {course} has no valid users in the DB. Skipping.".format(course=course)
                        print "*" * 50
                        continue
                    user_course_calculations = self.user_course_timespent(self.datamart_collection, course, user_by_course)
                    user_course_calculations = self.user_course_completion(self.datamart_collection, course,
                                                                           user_course_calculations, content)
                    user_course_calculations = self.user_course_engagement(self.datamart_collection, course,
                                                                           user_course_calculations, content)
                    user_course_calculations = user_course_calculations.drop('auth_id', 1)
                    columns_to_drop = [col for col in list(user_course_calculations) if "CONTENT" in col]
                    user_course_calculations_final = user_course_calculations.drop(columns_to_drop, axis=1)
                    user_course_calculations_final = user_course_calculations_final.dropna(axis=0,subset=['course_engagement','completion_rate'])
                    with open(self.csv, self.csv_mode if not first else 'w') as f:
                        user_course_calculations_final.to_csv(f, header=first, index=False)
                    first = False

                except:
                    logging.exception('\n\n%s did not go through. A report on course level failed to generate'%course)
                    errors.append(course)


        # File Level Calculations
        if self.file_level:
            print
            print "* " * 50
            print " Running file level dashboard calculations on the following courses:"
            for course in self.courses:
                print course
                try:
                    units, content = self.fetch_course_unit_content(mysql_connection, course)
                    if len(content) == 0:
                        print "*" * 50
                        print "Course {course} has no existing content in the DB. Skipping.".format(course=course)
                        print "*" * 50
                        continue
                    user_by_course, num_valid_users = self.user_course_list(mysql_connection, self.collection, course)
                    if num_valid_users < 1:
                        print "*" * 50
                        print "Course {course} has no valid users in the DB. Skipping.".format(course=course)
                        print "*" * 50
                        continue

                    user_content_calculations = self.user_content_timespent(self.datamart_collection, course, content,
                                                                            user_by_course)
                    user_content_calculations = self.user_content_completion(self.datamart_collection, course,
                                                                             user_content_calculations, content)
                    user_content_calculations = self.user_content_engagement(self.datamart_collection, course,
                                                                             user_content_calculations, content)
                    user_content_calculations = user_content_calculations.drop('auth_id', 1)
                    with open(self.csv, self.csv_mode) as f:
                        user_content_calculations.to_csv('{path}/file_level_{course}.csv'.format(path=self.path,course=course), header=True, index=False)

                except:
                    logging.exception('\n\n%s did not go through. A report on file level failed to generate'%course)
                    errors.append(course)
        return errors
