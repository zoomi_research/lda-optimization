__author__ = 'weiyuchen'

import sys
import json
import csv
import datetime
import unicodedata
import math
import operator
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot, draw, show


import functions as func
import quantizing
import boxplots


# import pickle
# with open('ITM_GL_GL_TA_005_1495008060000_clickstream', 'rb') as fp:
#     events = pickle.load(fp)

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'

# parameters
Thres_NumberRepetition = 2
max_sequence_gap_time = 10*60
Thres_HF = 2.0 # seconds.  for scrubbs within this threshold, we don't consider it as final. e.g. user is just searching in the video
THRES_GAP = 2.5 # if timestamps of playback events are 3 * slide_duration apart, then treat them as separate sequences
Thres_close_events = 0.5 # seconds
Thres_short_play = 1.0 # seconds. If a play happens right after a scrubb and is shorter than this. Exclude this Play.



'''/////////////////////////////////////////////////////////////////////////////////////////////////////////////////'''


class action():

    def __init__(self, from_pos, to_pos, from_time, to_time, action, vid, duration):
        self.from_pos = from_pos
        self.to_pos = to_pos
        self.from_time = from_time
        self.to_time = to_time
        self.action = action # action = Pl/Pa/Sf/Sb/....
        self.vid = vid # video ID
        self.duration = duration

    def traveled_video_length(self):
        # calculate the video length for certain action (Pl/Pa/Sf/Sb/...)

        if self.action == 'Play' or self.action == 'Scrubb':
            sign = 1.0
            diff = self.to_pos - self.from_pos
            if diff < 0:
                diff = -diff
                sign = -1.0
            return sign * math.ceil(diff/20.0)
        else:
            return 0

    def time_elapsed(self):
        # calculate the time spent for certain action (Pl/Pa/Sf/Sb/...)
        sign = 1.0
        diff = self.to_time - self.from_time
        if diff < 0:
            diff = -diff
            sign = -1.0
        return sign * math.ceil(diff/20.0)

    def t_for_C_Switch(self):
        if self.to_pos < self.duration:
            return 1
        else:
            return 2

def filter_out_scrubb_and_window_events(list_of_evts):
    # handle clickstream fire issue, do not consider these (duplicate) events
    new = []
    for evt in list_of_evts:
        if 'action' in evt and evt['action'] == 'Scrubb':
            continue
        elif evt['m_type'] == m_window:
            continue
        else:
            new.append(evt)

    return new

def handle_scrubbs_and_high_frequency_actions(action_list, Thres_HF, Thres_close_events, Thres_short_play, to_print):

    if len(action_list) < 3:
        return action_list

    # combine those continuous scrubbs that are very close to each other. iterate thru and combine step by step
    new = []
    for i, act in enumerate(action_list):

        if i == 0 or i == len(action_list) - 1: # do nothing for the last act
            new.append(act)
            continue

        if act.action == 'Scrubb' and new[-1].action == 'Scrubb':
            if too_close(act.from_time, new[-1].from_time, Thres_HF):
                new[-1].to_pos = act.to_pos
                new[-1].from_time = act.from_time
            else:
                new.append(act)
        else:
            new.append(act)


    action_list = new
    del new

    return action_list

def too_close(val1, val2, Thres):
    # if two actions happen too close, return TRUE
    if abs(val1 - val2) < Thres:
        return True
    else:
        return False

def divide_list_of_events_based_on_discontinuity(vID, vDur, evts_list):
    # sort list based on video ID, i.e., group multiple clickstream from different sessions based on one video ID
    all_clicktream_event_series = []

    ref_type = 'idle'  # 'Play', 'Pause'
    action_series = []
    for i, evt in enumerate(evts_list):
        if evt['m_type'] == m_video:

            if ref_type == 'idle':
                action_series.append(evt)
                ref_time = evt['timestamp']
                ref_type = evt['action']

            else:
                if (evt['timestamp'] - ref_time) > vDur * THRES_GAP:
                    all_clicktream_event_series.append( action_series )
                    ref_time = evt['timestamp']
                    ref_type = evt['action']
                    action_series = [evt]
                else:
                    action_series.append(evt)
                    ref_time = evt['timestamp']
                    ref_type = evt['action']

        elif evt['m_type'] == m_navi and evt['action'] in ['open', 'close']:
            action_series.append(evt)
            all_clicktream_event_series.append( action_series )
            ref_type = 'idle'
            action_series = []


    if len(action_series) > 0:
        all_clicktream_event_series.append( action_series )


    return all_clicktream_event_series


def compute_raw_action_list(vID, vDuration, events, user, to_print):

    all_clicktream_event_series = divide_list_of_events_based_on_discontinuity(vID, vDuration, events)

    all_action_sequences = []

    for evts_list in all_clicktream_event_series:
        sequence = []
        ref_type = 'idle'  # 'Play', 'Pause'

        # if to_print:
        #     print('---- clickstream after handling scrubb inconsistency ------')
        #     for x in evts_list:
        #         print(x['m_type'], x.get('duration'), x.get('video'), x.get('action'), x.get('timestamp'), x.get('position'), x.get('toPosition'))
        #     print('-----------\n')


        for i, evt in enumerate(evts_list):
            if evt['m_type'] == m_video and evt['action'] in ['Pause', 'Play', 'Scrubb']:

                if ref_type == 'idle':
                    ref_pos = evt['position']
                    if evt['action'] == 'Scrubb':
                        ref_to_pos = evt['toPosition']
                    else:
                        ref_to_pos = 0
                    ref_time = evt['timestamp']
                    ref_type = evt['action']
                else:
                    new_pos = evt['position']
                    new_time = evt['timestamp']
                    if ref_type == 'Scrubb':
                        new_act = action(ref_pos, ref_to_pos, ref_time, new_time, ref_type, vID, vDuration)
                        sequence.append(new_act)
                    elif ref_type == 'Pause':
                        new_act = action(ref_pos, ref_pos, ref_time, new_time, ref_type, vID, vDuration)
                        #sequence.append(new_act)
                    else:
                        new_act = action(ref_pos, new_pos, ref_time, new_time, ref_type, vID, vDuration)
                        #sequence.append(new_act)

                    ref_pos = evt['position']
                    if evt['action'] == 'Scrubb':
                        ref_to_pos = evt['toPosition']
                    else:
                        ref_to_pos = 0
                    ref_time = evt['timestamp']
                    ref_type = evt['action']

            elif evt.get('m_type') in [m_navi] and ref_type != 'idle':
                new_time = evt['timestamp']
                if ref_type == 'Play':
                    new_pos = min( (ref_pos + new_time-ref_time), vDuration)
                    new_act = action(ref_pos, new_pos, ref_time, new_time, ref_type, vID, vDuration)
                    #sequence.append(new_act)
                    #sequence.append(action(new_pos, new_pos, new_time, new_time, 'CS', vID, vDuration))
                #else:
                    #sequence.append(action(ref_pos, ref_pos, new_time, new_time, 'CS', vID, vDuration))



        if to_print:
            print('---- raw action sequence ----')
            for act in sequence:
                print(act.action, act.from_pos, act.to_pos, act.from_time)
            print('------\n')


        all_action_sequences.append(sequence)

    return all_action_sequences



def divide_evts_in_chapter_into_segments_for_single_user(list_of_evts):
    # input, per user raw clickstream events
    # output, per user, per chapter, raw clickstream events
    result = {}

    current_vid = 'fake'
    tmp = []
    for i, evt in enumerate(list_of_evts):
        if current_vid == 'fake' and evt['m_type'] == m_video and evt['video'] != current_vid:
            tmp.append(evt)
            current_vid = evt['video']
            result[current_vid] = []

        elif current_vid != 'fake':
            if evt['m_type'] == m_video and evt['video'] != current_vid:
                result[current_vid] += tmp
                tmp = [evt]
                current_vid = evt['video']
                result[current_vid] = []
            else:
                tmp.append(evt)

    if current_vid != 'fake' and len(tmp) > 0:
        result[current_vid] += tmp

    return result


###########################################################################################

def main(events):

    vIDs_durations = {}
    # duration for the same video may differ, select the duraction for video
    # u'ITM_GL_GL_TA_005_1495008060000_50517_CONTENT0': {191.467 sec: appeared 37 times}
    for evt in events:
        if 'video' in evt and 'duration' in evt:
            vID = evt['video']
            dura = evt['duration']
            if vID not in vIDs_durations:
                vIDs_durations[vID] = {dura: 1}
            else:
                if dura not in vIDs_durations[vID]:
                    vIDs_durations[vID][dura] = 1
                else:
                    vIDs_durations[vID][dura] += 1

    tmp = {}
    for id in vIDs_durations:
        du = max(vIDs_durations[id].iteritems(), key=operator.itemgetter(1))[0]
        tmp[id] = float(du)

    vIDs_durations = tmp
    del tmp

    # find all chapter names, and sort
    chap_names = []
    dates = []
    for evt in events:
        if 'chapter' in evt and evt['chapter'] not in chap_names:
            chap_names.append(evt['chapter'])
    chap_names = sorted(chap_names)
    print('chap_names: ', chap_names)

    uv = func.get_uv_dict(events) # group events (by user)
    uv = func.make_valid(uv) # validify user events
    users = sorted(uv.keys())

    actions_dict_user_vID_sequences = {}

    for user in users:

        actions_dict_user_vID_sequences[user] = {}

        all_evts_this_user = uv[user]
        chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts_this_user) # group by chapter

        for ch in chap_names:
            if ch not in chapters:
                continue

            evts_list = chapters[ch]

            video_evt_segments = divide_evts_in_chapter_into_segments_for_single_user(evts_list)

            for vID in video_evt_segments: # only execute once since chapters contain only one video

                actions_dict_user_vID_sequences[user][vID] = []

                all_action_sequences = compute_raw_action_list(vID, vIDs_durations[vID], video_evt_segments[vID], user, False)

                for sequence in all_action_sequences:

                    sequence = handle_scrubbs_and_high_frequency_actions(sequence, Thres_HF, Thres_close_events, Thres_short_play, False)

                    actions = []
                    for act in sequence:

                        tmp = {'action':act.action, 'time_elapsed': 0.0,'position': act.from_pos}
                        if act.action == 'Pause':
                            tmp['time_elapsed'] = act.time_elapsed()
                        elif act.action == 'CS':
                            tmp['time_elapsed'] = act.t_for_C_Switch()
                        elif act.action == 'Scrubb':
                            tmp['time_elapsed'] = act.traveled_video_length()
                            if tmp['time_elapsed'] >= 0:
                                tmp['action'] = 'SF'
                            else:
                                tmp['action'] = 'SB'
                                tmp['time_elapsed'] = 0 - tmp['time_elapsed']
                        else:
                            tmp['time_elapsed'] = act.traveled_video_length()
                        actions.append(tmp)


                    actions_dict_user_vID_sequences[user][vID].append(actions)


    # actions_dict_user_vID_sequences = quantize_action_elapsed_times(actions_dict_user_vID_sequences, vIDs_durations.keys())

    for user in actions_dict_user_vID_sequences:
        for vID in actions_dict_user_vID_sequences[user]:

            for acts in actions_dict_user_vID_sequences[user][vID]:
               # if len(acts) > 2:
                #if vID == debug_vid and user == debug_user:
                        # print('\n\nRESULT: ')
                        print(vID, user)
                        for act in acts:
                            print('%s   %s' %(act['action'], act['time_elapsed']))
                        print('--------')


    # with open('users_videoIDs_sequences.json', 'w') as fp:
    #     json.dump(actions_dict_user_vID_sequences, fp)

    return actions_dict_user_vID_sequences





