__author__ = 'WeiyuChen'

import pymongo
import MySQLdb
import json
from StringIO import StringIO
from configuration import *

def _do_connect(connection_name):
    options = DATABASES[connection_name]
    connection = pymongo.MongoClient(options['HOST'], options['PORT'])
    connection[options['NAME']].authenticate(options['USER'], options['PASSWORD'])

    return connection

def _do_connect_mysql(connection_name):
    options = DATABASES[connection_name]
    connection = MySQLdb.Connection(
        host=options['HOST'],
        user=options['USER'],
        passwd=options['PASSWORD'],
        port=options['PORT'],
        db=options['NAME']
    )

    return connection


def mongo_connect():
    return _do_connect('mongo_backend')

def mongo_datamart_connect():
    return _do_connect('mongo_datamart')

def mysql_connect():
    return _do_connect_mysql('mysql_backend')

