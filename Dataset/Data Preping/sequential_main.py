__author__ = 'WeiyuChen'
import pandas as pd
from database_connect import *
import pickle
import numpy as np
import save_motifs_to_json_video
import feature_based_motif_to_json_video
import feature_action_sequence_combination
import search_motifs_video
import courses
import toCombine
import combine_repetitive
import save_motifs_to_json_storyline

course_IDs = courses.course_IDs

def user_course(datamart_connection,course_ID):
    course_collection = datamart_connection['prod_datamart']['user_content_interactions']
    return list(course_collection.find({'course':course_ID}).distinct('user'))

''' run '''
def main():
    '''connecting to AWS'''
    connection = mongo_connect()
    print ' mongo backend connected '
    datamart_connection = mongo_datamart_connect()
    print ' mongo datamart connected '
    sql_connection = mysql_connect()
    print ' sql backend connected '

    combined_sequential = pd.DataFrame()

    for course_ID in course_IDs:
        users = user_course(datamart_connection, course_ID)
        events = list(connection['prod_backend']['measure_base'].find({'course': course_ID}))
        #events = connection['prod_backend']['measure_base'].find({'course': course_ID}).distinct('m_type')


        # from clickstream to action defined motif jason
        event_based_action_sequence = save_motifs_to_json_video.main(events)
        feature_based_action_sequence = feature_based_motif_to_json_video.main(datamart_connection, course_ID, users,
                                                                               connection)

        action_sequence = feature_action_sequence_combination.main(event_based_action_sequence,
                                                                   feature_based_action_sequence)


        # if we want timestamp associated, and properly grouped
        action_sequence = feature_action_sequence_combination.group_segment(action_sequence)
        # combine any consecutive duplicates
        action_sequence = combine_repetitive.main(action_sequence)

        with open('action_sequence_ITM_GL_GL_RRP_030_1491403560000.pickle', 'wb') as handle:
            pickle.dump(action_sequence, handle, protocol=pickle.HIGHEST_PROTOCOL)

        # search and output per user per motif count
        sequential = search_motifs_video.main_search_for_user_count(action_sequence)
        sequential['user_id'] = sequential.index
        sequential = sequential.rename(columns={'Unnamed: 0': 'user_id'})

        combined_sequential = toCombine.to_combine(combined_sequential,sequential)


    return combined_sequential

