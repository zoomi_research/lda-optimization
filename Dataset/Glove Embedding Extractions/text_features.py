import glob
import gensim
import string
import pandas as pd
from gensim import utils
from gensim.parsing.porter import PorterStemmer

def stem_text(text):
    """
    Return lowercase and (porter-)stemmed version of string `text`.
    """
    text = utils.to_unicode(text)
    p = PorterStemmer()
    return ' '.join(p.stem(word) for word in text.split())

def main():
    # define the result df
    embeddings = pd.DataFrame()
    # import data
    wavs = glob.glob("/Users/weiyuchen/Documents/Zoomi_Research/Data_Analysis/Prerequisite_Structure/ITM_GL_GL_RRP_040_1491419580000/OEBPS/**/*.txt",recursive=True)
    stopwords = gensim.parsing.preprocessing.STOPWORDS
    # word embeddings
    model_word2vec = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True) # pretrained word2vec
    gensim.scripts.glove2word2vec.glove2word2vec('glove.6B.100d.txt', 'glove_converted.txt')
    model_glove = gensim.models.KeyedVectors.load_word2vec_format('glove_converted.txt',binary=False)  # pretrained word2vec
    vocab = model_glove.vocab.keys()
    for wav in wavs:
        scripts = open(wav,"r").read()
        # remove stopwords
        scripts_nostopwords = [script for script in scripts.split(" ") if script not in stopwords]
        # stem words
        scripts = stem_text(' '.join(scripts_nostopwords))
        # remove numbers
        scripts = ' '.join([script for script in scripts.split(" ") if not script.isdigit()])
        # remove punctuations
        scripts = scripts.translate({ord(c): None for c in string.punctuation})
        # remove words not in vocab
        scripts = ' '.join([script for script in scripts.split(" ") if script in vocab])
        vectors = 0
        for word in scripts.split():
            vectors = vectors + model_glove[word]
        embeddings[wav.split('/')[-1]] = vectors

    return embeddings

embeddings = main()