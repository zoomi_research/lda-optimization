from watson_developer_cloud import SpeechToTextV1
import json
import glob
import wave
import contextlib
import io


# open .wav file and divide the number of frames by the framerate
def get_video_length(file):
    with contextlib.closing(wave.open(file, 'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)
        print(duration)

def extract_text(directory,out_dir):
    # set up the speect to text API with key and password
    stt = SpeechToTextV1(username="80758a08-08ef-4977-8e47-8a0b42c0e2b4", password="gu72xKmD6tNv")
    out_dir = "pmi_content"
    # grab all the pathnames matching pattern according to rules in Unix shell
    wavs = glob.glob("/Users/weiyuchen/Documents/Zoomi_Research/Data_Analysis/Prerequisite_Structure/ITM_GL_GL_RRP_040_1491419580000/OEBPS/**/*.wav",recursive=True)
    for wav in wavs:
        print(wav)
        #get_video_length(wav)
        audio_file = open(wav, "rb")

        # Call the Watson API
        results = json.loads(json.dumps(stt.recognize(audio_file, content_type="audio/wav", continuous=True),indent=2))
        print_str = ""
        print(results)
        for result in results["results"]:
            print(result)
            print_str += result['alternatives'][0]['transcript']
        with open(wav.split(".")[0]+".txt", "w") as text_file:
            text_file.write(print_str)
        print(print_str)


if __name__ == '__main__':
    extract_text

