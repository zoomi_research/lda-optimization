from pydub import AudioSegment
import glob
import math
import os

second_length = 1000 # works in milliseconds

wavs = glob.glob("/Users/weiyuchen/Documents/Zoomi_Research/Data_Analysis/Prerequisite_Structure/ITM_GL_GL_RRP_040_1491419580000/OEBPS/**/*.wav",recursive=True)
for wav in wavs:
    print(wav)
    original_wav = AudioSegment.from_wav(wav)
    iterations = math.ceil(len(original_wav)/(20*second_length))
    for i in range(iterations):
        new_audio = original_wav[second_length*i:min((i+20)*second_length,len(original_wav))]
        new_audio.export(wav.split('.')[0]+'_'+str(i)+'.wav',format="wav")
    os.remove(wav)

# # remove wronged wav
# wavs = glob.glob("/Users/weiyuchen/Documents/Zoomi_Research/Data_Analysis/Prerequisite_Structure/ITM_GL_GL_RRP_040_1491419580000/OEBPS/**/*.wav",recursive=True)
# for wav in wavs:
#     os.remove(wav)