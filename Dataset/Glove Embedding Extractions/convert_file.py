import subprocess
import os
import glob

def convert_file(video):
    print(video)
    path = os.path.abspath(video)
    output_path = path.split(".mp4")[0] + ".wav"
    command = "ffmpeg -i " + path + " -ab " \
        + " 160k -ac 2 -ar 44100 -vn " \
        + output_path
    subprocess.call(command, shell=True)

def convert_audio(audio):
    path = os.path.abspath(audio)
    output_path = path.split(".mp3")[0] + ".wav"
    command = "ffmpeg -i " + path + " " \
              + output_path
    subprocess.call(command, shell=True)





wavs = glob.glob("/Users/weiyuchen/Documents/Zoomi_Research/Data_Analysis/Prerequisite_Structure/ITM_GL_GL_RRP_040_1491419580000/OEBPS/**/*.mp4",recursive=True)
for wav in wavs:
    print(wav)
    convert_file(wav)