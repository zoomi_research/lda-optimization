import pandas as pd
import numpy as np
import pickle

def topic_deviation(doc_topic):
    return [np.std(i) for i in doc_topic]





if __name__ == '__main__':
    with open('course_lda.pickle', 'rb') as handle:
        [topic_word, doc_topic, topic_word_mapping] = pickle.load(handle)
