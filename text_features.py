import sklearn
import pandas as pd
import lda
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
import os
import seaborn as sns
import matplotlib.pyplot as plt
import pickle


horizontal = [8,14,22]

def perform_filtering(documents):
    # Aggressive stopword list (http://www.webconfs.com/stop-words.php)
    stopwords = open( 'stop_words.txt', 'r').readlines()
    stopwords = [words.strip('\n') for words in stopwords]

    # iterate over the dataframe
    # remove stop words, upper case letters
    for index, row in documents.iterrows():
        temp = documents.iloc[index,1].lower()
        temp = " ".join([word for word in temp.split() if word not in stopwords])
        documents.iloc[index, 1] = temp
    return documents


def perform_lda(documents):
    tf_vectorizer = CountVectorizer()
    tf = tf_vectorizer.fit_transform(documents['content'])
    tf_feature_names = tf_vectorizer.get_feature_names()

    model = lda.LDA(n_topics=10, n_iter=1500, random_state=1)
    model.fit(tf)
    [topic_word, doc_topic, nzw, ndz, nz] = [model.topic_word_, model.doc_topic_, model.nzw_, model.ndz_, model.nz_]

    topic_word_mapping = {}
    for i, topic_dist in enumerate(topic_word):
        topic_words = np.array(tf_feature_names)[np.argsort(topic_dist)][:-11:-1]
        topic_word_mapping[i] = topic_words

    return topic_word, doc_topic, topic_word_mapping


def perform_heatmap(doc_topic, horizontal):
    xticks = ['Topic ' + str(i + 1) for i in range(0, 10)]
    ax = sns.heatmap(doc_topic, yticklabels=5, xticklabels=xticks, cmap="Blues")
    for i in horizontal:
        plt.axhline(y=i, xmin=0, xmax=1, linewidth=1, color='k')
    return None

if __name__ == '__main__':
    with open('course_text.pickle', 'rb') as handle:
        documents = pickle.load(handle)
    filtered_documents = perform_filtering(documents)
    topic_word, doc_topic, topic_word_mapping = perform_lda(filtered_documents)
    with open('course_lda.pickle', 'wb') as handle:
        pickle.dump([topic_word, doc_topic, topic_word_mapping], handle, protocol=pickle.HIGHEST_PROTOCOL)



