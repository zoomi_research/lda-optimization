import pandas as pd
import numpy as np
import os
import glob
import pickle


def text_parsing( ):
    res = pd.DataFrame()
    files = glob.glob(r"C:\Users\Weiyu Chen\PycharmProjects\NTI_Skipping\Texts\**")
    file_names = [file.split('\\')[-1] for file in files]
    res['name'] = file_names
    reading_inputs = []
    for file in files:
        f = open(file).read()
        reading_inputs.append(f)
    res['content'] = reading_inputs
    return res

def skip_parsing():
    res = pd.DataFrame()
    motif = pd.read_csv(r'C:\Users\Weiyu Chen\PycharmProjects\NTI_Skipping\Dataset\user_motif.csv')
    motif.rename(index = str, columns = {motif.columns[0]: 'User'}, inplace=True)
    return res

if __name__ == '__main__':
    course_text = text_parsing( )
    with open('course_text.pickle', 'wb') as handle:
        pickle.dump(course_text, handle, protocol=pickle.HIGHEST_PROTOCOL)
