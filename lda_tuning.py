import pandas as pd
import numpy as np
import pickle
import gensim
import numpy as np
import seaborn as sns

from gensim.models import CoherenceModel, LdaModel, LsiModel, HdpModel
from gensim.models.wrappers import LdaMallet
from gensim.corpora import Dictionary
from gensim.utils import lemmatize
from pprint import pprint
import matplotlib.pyplot as plt
import pyLDAvis.gensim



def evaluate_graph(dictionary, corpus, texts, limit):
    """
    Function to display num_topics - LDA graph using c_v coherence

    Parameters:
    ----------
    dictionary : Gensim dictionary
    corpus : Gensim corpus
    limit : topic limit

    Returns:
    -------
    lm_list : List of LDA topic models
    c_v : Coherence values corresponding to the LDA model with respective number of topics
    """
    c_v = []
    lm_list = []
    for num_topics in range(1, limit):
        lm = LdaModel(corpus=corpus, num_topics=num_topics, id2word=dictionary)
        lm_list.append(lm)
        cm = CoherenceModel(model=lm, texts=texts, dictionary=dictionary, coherence='c_v')
        c_v.append(cm.get_coherence())

    # Show graph
    x = range(1, limit)
    plt.plot(x, c_v)
    plt.xlabel("num_topics")
    plt.ylabel("Coherence score")
    plt.legend(("c_v"), loc='best')
    plt.show()

    return lm_list, c_v

def perform_filtering(documents):
    # Aggressive stopword list (http://www.webconfs.com/stop-words.php)
    stopwords = open( 'stop_words.txt', 'r').readlines()
    stopwords = [words.strip('\n') for words in stopwords]

    # iterate over the dataframe
    # remove stop words, upper case letters
    for index, row in documents.iterrows():
        temp = documents.iloc[index,1].lower()
        temp = " ".join([word for word in temp.split() if word not in stopwords])
        documents.iloc[index, 1] = temp
    return documents


def evaluate_graph(dictionary, corpus, texts, limit):
    """
    Function to display num_topics - LDA graph using c_v coherence

    Parameters:
    ----------
    dictionary : Gensim dictionary
    corpus : Gensim corpus
    limit : topic limit

    Returns:
    -------
    lm_list : List of LDA topic models
    c_v : Coherence values corresponding to the LDA model with respective number of topics
    """
    c_v = []
    lm_list = []
    for num_topics in range(1, limit):
        lm = LdaModel(corpus=corpus, num_topics=num_topics, id2word=dictionary)
        lm_list.append(lm)
        cm = CoherenceModel(model=lm, texts=texts, dictionary=dictionary, coherence='c_v')
        c_v.append(cm.get_coherence())

    # Show graph
    x = range(1, limit)
    plt.plot(x, c_v)
    plt.xlabel("num_topics")
    plt.ylabel("Coherence score")
    plt.legend(("c_v"), loc='best')
    plt.show()

    return lm_list, c_v

def continuityModel(model, texts, dictionary):

    return 0

def evaluate_graph_continuity(dictionary, corpus, texts, limit):
    """
    Function to display num_topics - LDA graph using c_v coherence

    Parameters:
    ----------
    dictionary : Gensim dictionary
    corpus : Gensim corpus
    limit : topic limit

    Returns:
    -------
    lm_list : List of LDA topic models
    c_v : continuity values corresponding to the LDA model with respective number of topics
    """
    c_v = []
    lm_list = []

    for num_topics in range(1, limit):
        lm = LdaModel(corpus=corpus, num_topics=num_topics, id2word=dictionary)
        lm_list.append(lm)
        cm = CoherenceModel(model=lm, texts=texts, dictionary=dictionary, coherence='c_v')
        c_v.append(cm.get_coherence())

    return lm_list, c_v


if __name__ == '__main__':
    with open('course_text.pickle', 'rb') as handle:
        documents = pickle.load(handle)
    filtered_documents = perform_filtering(documents)
    train_texts = list(filtered_documents['content'])
    train_texts = [i.split() for i in train_texts]
    dictionary = Dictionary(train_texts)
    corpus = [dictionary.doc2bow(text) for text in train_texts]

    # lda
    ldamodel = LdaModel(corpus=corpus, num_topics=10, id2word=dictionary)
    pyLDAvis.enable_notebook()
    pyLDAvis.gensim.prepare(ldamodel, corpus, dictionary)
    ldatopics = ldamodel.show_topics(formatted=False)

    # lda coherence
    lmlist, c_v = evaluate_graph(dictionary=dictionary, corpus=corpus, texts=train_texts, limit=10)
    pyLDAvis.gensim.prepare(lmlist[2], corpus, dictionary)

    # HDP
    hdpmodel = HdpModel(corpus=corpus, id2word=dictionary)
    hdpmodel.show_topics()

    # Segment-difference

